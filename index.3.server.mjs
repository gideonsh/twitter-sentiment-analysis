/**
 * Step 3 - node native tcp server
 */

import log from '@ajar/marker'
import net from 'net'
import Twitter from 'twitter'
import Sentiment from 'sentiment'

const {consumer_key,consumer_secret,access_token_key,access_token_secret} = process.env;
const credentials = { consumer_key, consumer_secret, access_token_key, access_token_secret}
const client = new Twitter(credentials);
const sentiment = new Sentiment();

const { port } = process.env;

const server = net.createServer();

server.on('connection', socket => {

  log.yellow('✨ client connected ✨');

  socket.on('data', user => {
    log.blue('-> incoming:', user.toString().trim());
    let twitCount = 0;
    let totalSentiments = 0;
    let highest = 0;
    let lowest = 0;
    //filter relevant tweets
    const t =  client.stream('statuses/filter', {track: `${user}`});

    //for each tweet
    t.on('data', twit => {
      if(twit.lang == 'en'){

        const {score} = sentiment.analyze(twit.text);

        twitCount++;
        totalSentiments += score;

        if(score > highest) highest = score;
        if(score < lowest) lowest = score;

        const avg = totalSentiments / twitCount;

        //log.v(`${twitCount}`,`score ${score}\t|\tavg ${avg.toFixed(2)}\t|\thigh: ${highest} \t|\tlow: ${lowest}`);
        console.log(`twit ${twit}`);
        console.log(`score ${score}`);
        console.log(`avg ${avg}`);
        socket.write(JSON.stringify({twit,score,avg}));        
      }
    });
 
      t.on('error', error => {
      log.error(error);
      });

  });

  socket.on('end', () => {
    // log.red('client disconnected');
    log.yellow('✨ client disconnected ✨');
    clearInterval(intervalID)
  }); 

  let count = 0
  let intervalID = setInterval( _ => {
    socket.write(`${ JSON.stringify( { value: count++ } ) }\n` )
  }, 100)

  // socket.pipe(socket);
});

server.on('error', err => log.error(err));
server.listen(port, () => log.v('✨ ⚡Server is up  🚀'));