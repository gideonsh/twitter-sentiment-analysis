/**
 * Step 3 - node native tcp client
 */
import log from '@ajar/marker'
import net from 'net'

const { port } = process.env;

const socket = net.createConnection({ port }, _ => {
   log.yellow('✨ ⚡connected to server! ⚡✨');
   socket.write('{"initial":"msg, client -> server..."}\n');
});

socket.on('data', buffer => {
    
  let result = JSON.parse(buffer.toString());
  const {avg,twit,score} = result;
  console.log(`tweet ${avg} ${twit} ${score}`);
});
  
socket.on('end', () => { 
  log.yellow('✨ ⚡disconnected from server ⚡✨');
});